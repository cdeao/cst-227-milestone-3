/* Cameron Deao
 * CST-227
 * James Shinevar
 * 6/22/2019 
 * Repo: https://bitbucket.org/cdeao/cst-227-milestone-3/src/master/ */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cameron_Deao_Milestone_2
{
    //Interface created with a single method.
    interface IPlayable
    {
       void playGame();
    }
}
