/* Cameron Deao
 * CST-227
 * James Shinevar
 * 6/22/2019 
 * Repo: https://bitbucket.org/cdeao/cst-227-milestone-3/src/master/ */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cameron_Deao_Milestone_2
{
    //MinesweeperGame class extends GamesBoard and implements IPlayable.
    class MinesweeperGame : GameBoard, IPlayable
    {
        //Variables used throughout the class.
        public int colChoice;
        public int rowChoice;
        public int selectionCounter;
        public bool playerDead = false;
        public bool validSelection = false;
        public bool visitedCell = false;

        //Implementing the playGame method.
        public void playGame()
        {
            //The while-loop will continue allowing the user to play until they
            //either hit a live cell or they hit the total number of safe cells.
            while (playerDead == false && selectionCounter != totalSafeCells)
            {
                //Exception handling for the user input of row and column choice.
                try
                {
                    Console.WriteLine("Enter a row choice: ");
                    rowChoice = int.Parse(Console.ReadLine());
                    Console.WriteLine("Enter a col choice: ");
                    colChoice = int.Parse(Console.ReadLine());
                }
                catch(FormatException)
                {
                    Console.WriteLine("That is not a valid input.");
                    continue;
                }
                //Checking if the choice is within the index of the array.
                if (rowChoice >= 0 && rowChoice < board.GetLength(0) && colChoice >= 0 && colChoice < board.GetLength(1))
                {
                    //Fires if bool that checks if a cell has been visited it true.
                    if (visitedCell == true)
                    {
                        Console.Clear();
                        DisplayBoard(board);
                        Console.WriteLine("Cell has already been visited.");
                        Console.Write(Environment.NewLine);
                    }
                    //Calling the recursive method if a valid choice is input.
                    Recursive(rowChoice, colChoice);
                    //Resetting two bools.
                    visitedCell = false;
                    validSelection = true;
                    //Fires if the player hits a live cell.
                    if (board[rowChoice, colChoice].Live == true)
                    {
                        //Setting the game loss condition with a bool.
                        playerDead = true;
                    }
                    //Fires if the player has selected a cell that has 
                    //been visited already.
                    if(board[rowChoice,colChoice].VisitedCell == true)
                    {
                        //Sets a bool to true if the cell has been visited.
                        visitedCell = true;
                    }
                    //No longer setting the cell as visited from this section of code.
                    //The cell is now set to true in the recursive method.
                    //Fires if non of the prior checks succeed.
                    //else
                    //{
                    //    //Setting the chosen cell to visited.
                    //    board[rowChoice, colChoice].VisitedCell = true;
                    //}
                }
                //Fires if the player has not died and they input a 
                //valid cell choice.
                if (playerDead == false && validSelection == true)
                {
                    Console.Clear();
                    DisplayBoard(board);
                    selectionCounter++;
                }
                
                //Fires if the win condition is met for the game.
                if(selectionCounter == totalSafeCells)
                {
                    Console.Clear();
                    DisplayBoard(board);
                    Console.WriteLine("Congratulations you win!");
                    Console.Write(Environment.NewLine);
                }
                //Fires if no valid selection was input.
                else if(validSelection == false)
                {
                    Console.Clear();
                    DisplayBoard(board);
                    Console.WriteLine("Please enter valid coordinates to play!");
                    Console.Write(Environment.NewLine);
                }
            }
            //Fires if the player has lost the game.
            if (playerDead)
            {
                revealBoard = true;
                Console.Clear();
                DisplayBoard(board);
                Console.Write(Environment.NewLine);
                Console.WriteLine("GAME OVER!");
            }
        }

        public void Recursive(int rowChoice, int colChoice)
        {
            //Checking the current cell to see if it has live neighbors and it isn't visited yet.
            if(board[rowChoice, colChoice].NeighborsLive == 0 && board[rowChoice, colChoice].VisitedCell == false)
            {
                //Setting the cell as visited.
                board[rowChoice, colChoice].VisitedCell = true;
                //These four if-statement check in all four directions their neighbor cells.
                if (rowChoice - 1 >= 0 && board[rowChoice - 1, colChoice].Live == false)
                {
                    //Incrementing the win condition counter.
                    selectionCounter++;
                    //Recursively passing in the new cell.
                    Recursive(rowChoice - 1, colChoice);
                }
                if (rowChoice + 1 < board.GetLength(0) && board[rowChoice + 1, colChoice].Live == false)
                {
                    selectionCounter++;
                    Recursive(rowChoice + 1, colChoice);
                }
                if (colChoice + 1 < board.GetLength(1) && board[rowChoice, colChoice + 1].Live == false)
                {
                    selectionCounter++;
                    Recursive(rowChoice, colChoice + 1);
                }
                if (colChoice - 1 >= 0 && board[rowChoice, colChoice - 1].Live == false)
                {
                    selectionCounter++;
                    Recursive(rowChoice, colChoice - 1);
                }
            }
            //Setting the current cell to visited.
            else
            {
                board[rowChoice, colChoice].VisitedCell = true;
            }
        }
    }
}