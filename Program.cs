/* Cameron Deao
 * CST-227
 * James Shinevar
 * 6/22/2019 
 * Repo: https://bitbucket.org/cdeao/cst-227-milestone-3/src/master/ */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cameron_Deao_Milestone_2
{
    class Program
    {
        //Driver class that calls upon each function.
        static void Main(string[] args)
        {
            MinesweeperGame mg = new MinesweeperGame();
            mg.EstablishBoard(9);
            mg.ActivateCell(9);
            mg.SetCount();
            mg.DisplayBoard(mg.board);
            mg.playGame();
            Console.ReadKey();
        }
    }
}